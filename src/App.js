import React from 'react';
import './App.css';
import Chat from './components/Chat/Chat'

class App extends React.Component {
  render() {
    return <Chat url='https://edikdolynskyi.github.io/react_sources/messages.json' />
  }
}

export default App;