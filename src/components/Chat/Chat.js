import React from 'react'
import MessageList from '../MessageList/MessageList'
import MessageInput from '../MessageInput/MessageInput';
import Header from '../Header/Header';

const user = {
    userId: 5,
    user: 'Alex'
};
class Chat extends React.Component {
    state = {
        messages: [],
        user: user,
        message: {}
    }

    componentDidMount() {
        fetch(this.props.url).then(res => res.json()).then(res => this.setState({ messages: res }));
    }
    onNewMessage = (message) => {
        this.setState({ messages: [...this.state.messages, message], user: user, message: {} })
        console.log(this.state);
    }
    onDelete = (message) => {
        let newMessagesArray = this.state.messages.filter(item => item.id !== message.id);
        this.setState({ messages: [...newMessagesArray], user: user, message: {} });
    }
    onLike = (message) => {
        let newMessagesArray = this.state.messages.map(item => {
            if (item.id === message.id) {
                item = {
                    ...item,
                    isLiked: item.isLiked === true ? false : true
                }
            }
            return item;
        });

        this.setState({ messages: [...newMessagesArray], user: user, message: {} });

    }
    onEdit = (message) => {
        this.setState({ messages: this.state.messages, user: user, message: message });
        // console.log(this.state);
    }
    onUpdateMessage = (message) => {
        let newMessagesArray = this.state.messages.map(item => {
            if (item.id === message.id) {
                item = message
            }
            return item;
        });
        this.setState({ messages: [...newMessagesArray], user: user, message: {} })
        // console.log(this.state);
    }
    render() {
        return <div>
            <Header messages={this.state.messages} />
            <MessageList messages={this.state.messages} user={this.state.user} onDelete={this.onDelete} onLike={this.onLike} onEdit={this.onEdit} />
            <MessageInput onSubmit={this.onNewMessage} user={this.state.user} message={this.state.message} onUpdateMessage={this.onUpdateMessage} />
        </div>
    }
}

export default Chat;