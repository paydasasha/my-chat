import React from 'react'
import './Header.css'

function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

class Header extends React.Component {

    getUsersCount = () => {
        let data = this.props.messages;
        let users = data.map(msg => {
            return msg.userId
        });
        console.log(users);
        let unique = users.filter(onlyUnique);
        let usersLength = unique.length;
        return usersLength;
    }

    getTime = (date) => {
        let formatted = new Date(date);
        return `${formatted.getHours()}:${formatted.getMinutes()}`
    }
    lastMessage = Number(this.props.messages.length) - 1;

    render() {
        return <div className="header" >
            <div className="header-title">SuperChat</div>
            <div className="header-users-count">{this.getUsersCount()} users</div>
            <div className="header-messages-count">{this.props.messages.length} msgs</div>
            {/* <div className="header-last-message-date">last msg - {this.getTime(this.props.messages[this.lastMessage].createdAt)}</div> */}
        </div>
    }
}
export default Header;