import React from 'react';
import './Message.css';

class Message extends React.Component {
    getTime = (date) => {
        let formatted = new Date(date);
        return `${formatted.getHours()}:${formatted.getMinutes()}`
    }
    onSubmit = () => {
        this.props.onDelete(this.props.message);
        // console.log(this.props.message.id);
    }
    onLike = () => {
        this.props.onLike(this.props.message);
    }
    onEdit = () => {
        this.props.onEdit(this.props.message);
    }
    isOwnMessage = () => this.props.user.userId === this.props.message.userId;
    isLiked = () => this.props.message.isLiked === true;
    messageIsUpdated = () => this.props.message.updatedAt ? this.props.message.updatedAt : this.props.message.createdAt;
    render() {

        return <div className={this.isOwnMessage() ? 'own-message' : 'message'}>
            <img className={this.isOwnMessage() ? 'display-none' : 'message-user-avatar'} src={this.props.message.avatar} alt='' />
            <div className='message-text'>{this.props.message.text}</div>
            <div className='message-time'>{this.getTime(this.messageIsUpdated())}</div>
            <div className='message-user-name'>{this.props.message.user}</div>
            <button type='button' onClick={this.onLike} className={this.isLiked() ? 'message-liked' : 'message-like'}>like</button>
            <button type='button' onClick={this.onSubmit} className={this.isOwnMessage() ? 'message-delete' : 'display-none'} >DEL</button>
            <button type='button' onClick={this.onEdit} className={this.isOwnMessage() ? 'message-edit' : 'display-none'} >EDIT</button>
        </div>
    }
}
export default Message;