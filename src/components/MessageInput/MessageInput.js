import React from 'react'
import './MessageInput.css'

class MessageInput extends React.Component {

    state = { message: { id: '' }, value: '' };

    static getDerivedStateFromProps(props, state) {
        console.log(props);
        if (state.message.id !== props.message.id) {
            return {
                message: props.message,
                value: props.message.text
            }
        } else {
            return null;
        }
    }

    onChange = (e) => { this.setState({ value: e.target.value }) };

    onSubmit = () => {
        if (!this.props.message?.id) {
            this.props.onSubmit({
                text: this.state.value,
                createdAt: Date().toString(),
                avatar: 'https://api-private.atlassian.com/users/8f525203adb5093c5954b43a5b6420c2/avatar',
                id: Date.now(),
                userId: this.props.user.userId,
                user: this.props.user.user
            })

        } else {
            this.props.onUpdateMessage({
                ...this.state.message,
                text: this.state.value,
                updatedAt: Date().toString()
            });
            this.setState({ value: '' });

        }
        this.setState({ value: '' })
    }

    render() {
        return <div className="message-input">
            <input className="message-input-text" type='text' value={this.state.value} onChange={this.onChange}></input>
            <button className="message-input-button" type='button' onClick={this.onSubmit}>Send</button>
        </div>
    }
}
export default MessageInput;