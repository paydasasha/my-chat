import React from 'react'
import Message from '../Message/Message'
import moment from 'moment'
import './MessageList.css'

class MessageList extends React.Component {
    renderMessages = () => {
        const dates = this.props.messages.reduce((acc, message) => {
            const date = moment(message.createdAt).format('DD MMM');
            if (!acc[date]) {
                acc[date] = [];
            }
            acc[date].push(message)
            return acc;
        }, {})
        console.log(dates);

        const days = Object.keys(dates);

        const messages = days.reduce((acc, day) => {
            acc.push({ type: 'separator', day })
            return acc.concat(dates[day])
        }, [])
        // console.log(messages);

        return messages.map((message) => {
            if (message.type === 'separator') {
                return <div className='messages-divider' key={message.day}>{message.day}</div>
            }
            return <Message key={message.id} message={message} user={this.props.user} onDelete={this.props.onDelete} onLike={this.props.onLike} onEdit={this.props.onEdit} />
        })


    }
    render() {
        return <div className='message-list'>{this.renderMessages()}</div>

    }
}
export default MessageList;